<?php
session_start();
include_once('../../../vendor/autoload.php.');
use App\Bitm\SEIP126882\Book\Book;
use App\Bitm\SEIP126882\Utility\Utility;
use App\Bitm\SEIP126882\Message\Message;


$book= new Book();
$allBook=$book->index();
//Utility::d($allBook);
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
  <h2>Book List</h2>

  <a href="create.php" class="btn btn-info" role="button">Add Book Title</a>  <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
  <br>
  <div id="message">

  <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
    echo Message::message();
  }?>
  </div>
    <div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th>SL#</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $sl=0;
    foreach($allBook as $book){
        $sl++;
        ?>
      <tr>
        <td><?php echo $sl; ?></td>
        <td><?php echo $book['id'] // for object: $book->id ; ?></td>
        <td><?php echo $book['title'] // for object: $book->title; ?></td>
        <td>
            <a href="view.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
            <a href="edit.php?id=<?php echo $book['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
            <a href="delete.php?id=<?php echo $book['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
            <a href="trash.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
        </td>


      </tr>
    <?php } ?>


    </tbody>
  </table>
  </div>
</div>
<script>
  $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>
