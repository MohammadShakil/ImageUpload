<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP126882\Utility\Utility;
use App\Bitm\SEIP126882\ProfilePicture\ImageUploader;

//Utility::d($_POST);

$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_POST)->view();

if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB21/Resources/Images/'.$singleInfo['images']);
    move_uploaded_file($temporary_location,'../../../Resources/Images/'.$imageName);


    $_POST['image']=$imageName;


}
$profilePicture= new ImageUploader();
$profilePicture->prepare($_POST)->update();

//Utility::d($_POST);
